# Tutorial Overview
This repo is a brief tutorial to get you started with Xilinx's Alveo u280 with Vitis. The content of the repo is mainly based on several Xilinx's documents, guides and tutorials. Refer to the bottom of the README.md to find links to the source. 

The tutorial's objective is to get you familiarised with the Vitis flow for executing FPGA binaries in Alveo U280. In this tutorial,  A simple vector kernel will be into a Xilinx Object (XO) file, verified using software and hardware emulation, and finally deployed in hardware.

## Softwares required for this tutorial

1. Xilinx Vivado/Vitis 2019.2 (The latest version is 2020.2. However, the tutorial content was tested and verified with the 2019.2 version). You can get it from [here]( https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis/2019-2.html.)
2. Xilinx Run Time (XRT).
3. Deployment Target Platform for U280.
4. Development Target Platform for U280.
5. You can download the files for 2), 3) and 4) from [here](https://www.xilinx.com/products/boards-and-kits/alveo/u280.html#vitis).
    * Navigate to the bottom of the linked website
    * Choose Vitis Design Flow
    * Click on Archive and choose 2019.2
    * Choose Ubuntu as the operating system
    * Choose the version installed on your PC
6. If not pre-installed,  download and Install all of the above softwares

## Hardware required for this tutorial
1. Xilinx Alveo U280. It is installed in Rocinante system.


## Writing a Kernel in C++

Just like HLS, C/C++ can be used to model kernels. Much of the syntax carries over from HLS. The following code is an example vector program that computes `y[i] = a*x[i]*x[i]+ 2*a*x[i]+10*a`.

```C++
void vpoly(
        const unsigned int *in1, // Read-Only Vector 1
        unsigned int *out,       // Output Result
        int size,                   // Size in integer
        int poly_scale
        )
{

#pragma HLS INTERFACE m_axi port=in1  offset=slave bundle=gmem
#pragma HLS INTERFACE m_axi port=out offset=slave bundle=gmem
#pragma HLS INTERFACE s_axilite port=in1  bundle=control
#pragma HLS INTERFACE s_axilite port=out bundle=control
#pragma HLS INTERFACE s_axilite port=size bundle=control
#pragma HLS INTERFACE s_axilite port=poly_scale bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

    unsigned int v1_buffer[BUFFER_SIZE];    // Local memory to store vector1
    unsigned int vout_buffer[BUFFER_SIZE];  // Local Memory to store result


    for(int i = 0; i < size;  i += BUFFER_SIZE)
    {
        int chunk_size = BUFFER_SIZE;
        //boundary checks
        if ((i + BUFFER_SIZE) > size) 
            chunk_size = size - i;

        read1: for (int j = 0 ; j < chunk_size ; j++){
            v1_buffer[j] = in1[i + j];
        }

        vadd: for (int j = 0 ; j < chunk_size; j ++){
        #pragma HLS PIPELINE II=1
            vout_buffer[j] = poly_scale*v1_buffer[j]*v1_buffer[j]+poly_scale*2*v1_buffer[j]+poly_scale*10; 
        }
        write: for (int j = 0 ; j < chunk_size ; j++){
            out[i + j] = vout_buffer[j];
        }
    }
 }
```

The code reads in1 array, performs `out[i] = (poly_scale*in1[i]*in1[i])+(2*poly_scale*in1[i])+(10*poly_scale)` and writes it back to the memory. This is performed in bursty fashion to improve memory performance. 

`in1` and `out` are mapped to `m_axi` port through which the PE can read and write to the memory. This is done using `#pragma HLS INTERFACE m_axi` pragma.Likewise the scalar arguements are mapped to `s_axilite` interface, through which the host provides values for those arguments. Vitis requires a control `s_axilite` interface for the kernel and all arguments. And a kernel can have only one AXI-Lite interface,  so all `s_axilite` pragmas must use the same `bundle` name. 


You can read more about the pragmas and optimisation from Vitis C/C++ kernel documentation from [here](https://www.xilinx.com/html_docs/xilinx2020_2/vitis_doc/tfo1593136615570.html) and [here](https://www.xilinx.com/html_docs/xilinx2020_2/vitis_doc/devckernels.html#rjk1519742919747). 

## Compiling, Linking, and Running the Application

### Software emulation

Before running the design on hardware, run the design in software emulation mode to verify functionality. To do this, from the parent folder type `./sw_emu.sh` in terminal. The contents of `./sw_emu.sh` are shown below.

```bash
$ source /opt/Xilinx/Vitis/2019.2/settings64.sh
$ source /opt/xilinx/xrt/setup.sh
$ mkdir -p sw_emu
$ cd sw_emu
$ g++ -I$XILINX_XRT/include/ -I$XILINX_VIVADO/include/ -Wall -O0 -g -std=c++11 ../code/host.cpp  -o 'host'  -L$XILINX_XRT/lib/ -lOpenCL -lpthread -lrt -lstdc++
$ v++ -c -t sw_emu --config ../code/design.cfg  -k vadd -I'../../code/' -o'vadd.xo' '../$ code/vadd.cpp'
$ v++ -l -t sw_emu --config ../code/design.cfg  -o'vadd.xclbin' vadd.xo
$ emconfigutil --platform xilinx_u280_xdma_201920_1
$ export XCL_EMULATION_MODE=sw_emu  
$ ./host vadd.xclbin
```

The source commands configures the environment to run Vitis. Whereas `g++` command compiles the host code. `v++ -c` command compiles the source code into a compiled kernel object (.xo file). And `v++ -l` links the compiled kernel with the target platform (sepcified in `/code/design.cfg`) and generates the FPGA binary (.xclbin file). The `-t` option of the v++ tool specifies the build target. Here it is set to sw_emu as we are building for software emulation. `emconfigutil` generates an emulation configuration file which defines the device type and quantity of devices to emulate for the specified platform.
 
Once the linking is done, the script launches the emulation. If successful, you should see the following message

```
INFO: Found Xilinx Platform
INFO: Loading 'vadd.xclbin'
TEST PASSED
```

The configuration `/code/design.cfg` file (shown below) is used to specify, amongst others, the name of the targeted platform and the mapping of kernel arguments to HBM banks. This can be edited to map arguments to differnt HBM ports if requried.

```
platform=xilinx_u280_xdma_201920_1
debug=1

[connectivity]
nk=vadd:1:vadd_1
sp = vadd_1.in1:HBM[0] 
sp = vadd_1.out:HBM[1] 
```

### Hardware emulation

Similar to software emulation, you can run hardware emulation by running `./hw_emu.sh` from the parent folder. The contents of the `hw_emu.sh` are same as `./sw_emu.sh` except for the target. In `hw_emu.sh`  -t is changed from sw_emu to hw_emu. All other options remain identical. The build will take anywhere from 10 to 15 mintues. Once done, the script launchs hardware emulation. If successful, you should see a message similar to the one shown below.

```
Found Platform
Platform Name: Xilinx
INFO: Reading vadd.xclbin
Loading: 'vadd.xclbin'
INFO: [HW-EM 01] Hardware emulation runs simulation underneath. Using a large data set will result in long simulation times. It is recommended that a small dataset is used for faster execution. The flow uses approximate models for DDR memory and interconnect and hence the performance data generated is approximate.
TEST PASSED
INFO: [Vitis-EM 22] [Wall clock time: 16:39, Emulation time: 0.108767 ms] Data transfer between kernel(s) and global memory(s)
vadd_1:m_axi_gmem-DDR[1]          RD = 32.000 KB              WR = 16.000 KB
```

### Hardware execution
Once the kernel is functionally verified, you can now run the kernel in hardware. To do that, run `./hw.sh`. This script is similar to previous scripts, but it targerts hardware (`-t hw`). If successful, you should see the following message

```
INFO: Found Xilinx Platform
INFO: Loading 'vadd.xclbin'
TEST PASSED
```

## Sources
https://github.com/Xilinx/Vitis-Tutorials (2019.2 branch)

https://www.xilinx.com/html_docs/xilinx2020_2/vitis_doc/

https://www.xilinx.com/support/documentation/boards_and_kits/accelerator-cards/2019_1/ug1301-getting-started-guide-alveo-accelerator-cards.pdf
