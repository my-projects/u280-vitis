#!/bin/bash
source /opt/Xilinx/Vitis/2019.2/settings64.sh
source /opt/xilinx/xrt/setup.sh
mkdir -p hw
cd hw
g++ -I$XILINX_XRT/include/ -I$XILINX_VIVADO/include/ -Wall -O0 -g -std=c++11 ../code/host.cpp  -o 'host'  -L$XILINX_XRT/lib/ -lOpenCL -lpthread -lrt -lstdc++
v++ -c -t hw --config ../code/design.cfg  -k vadd -I'../../code/' -o'vadd.xo' '../code/vadd.cpp'
v++ -l -t hw --config ../code/design.cfg  -o'vadd.xclbin' vadd.xo
	
unset XCL_EMULATION_MODE

./host vadd.xclbin