#!/bin/bash
source /opt/Xilinx/Vitis/2019.2/settings64.sh
source /opt/xilinx/xrt/setup.sh
mkdir -p hw_emu
cd hw_emu
g++ -I$XILINX_XRT/include/ -I$XILINX_VIVADO/include/ -Wall -O0 -g -std=c++11 ../code/host.cpp  -o 'host'  -L$XILINX_XRT/lib/ -lOpenCL -lpthread -lrt -lstdc++
v++ -c -t hw_emu --config ../code/design.cfg  -k vadd -I'../../code/' -o'vadd.xo' '../code/vadd.cpp'
v++ -l -t hw_emu --config ../code/design.cfg  -o'vadd.xclbin' vadd.xo
	
emconfigutil --platform xilinx_u280_xdma_201920_1
export XCL_EMULATION_MODE=hw_emu  

./host vadd.xclbin