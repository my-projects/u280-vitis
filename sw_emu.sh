#!/bin/bash
source /opt/Xilinx/Vitis/2019.2/settings64.sh
source /opt/xilinx/xrt/setup.sh
mkdir -p sw_emu
cd sw_emu
g++ -I$XILINX_XRT/include/ -I$XILINX_VIVADO/include/ -Wall -O0 -g -std=c++11 ../code/host.cpp  -o 'host'  -L$XILINX_XRT/lib/ -lOpenCL -lpthread -lrt -lstdc++
v++ -c -t sw_emu --config ../code/design.cfg  -k vadd -I'../../code/' -o'vadd.xo' '../code/vadd.cpp'
v++ -l -t sw_emu --config ../code/design.cfg  -o'vadd.xclbin' vadd.xo
	
emconfigutil --platform xilinx_u280_xdma_201920_1
export XCL_EMULATION_MODE=sw_emu  

./host vadd.xclbin